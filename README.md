# Aubergine Assignment

Covid node server-app 

# covid-api

REST services for Covid project

**Prerequisites:**

        node
        npm
        mongoDB

**Project Setup:**

1.  Go to aubergine-assignment gitlab repository and clone the repository or download it.

2.  Open aubergine-assignment folder which you just cloned/downloaded.

3.  Install required npm packages using the command mentioned below:

         $ npm install

4.  Start server using the command mentioned below:

         $ nodemon
5.  Once above command gets executed successfully server will get successfully deployed.

**Use Email Services**

1. Open src/shared/config.json to edit it.

2. Update SENDER_EMAIL_ID and SENDER_EMAIL_PASSWORD to the gmail-id you want to send the emails from (It must be gmail).

3. Go to [this link](https://myaccount.google.com/lesssecureapps) and grant access for less secure apps to send emails.

**Apis:**

<details><summary>Add user</summary>

* **URL**

  http://localhost:4000/user

* **Method:**

  `POST`

* **Data Params:**

  `firstName=[string]`
  `lastName=[string]`
  `email=[string]`
  `password=[string]`
  `country=[string]`
</details>
<details><summary>Fetch covid data / Get email</summary>

* **URL:**

  http://localhost:4000/covid

* **Method:**

  `GET`

* **Data Params:**

        - Required:
           `email=[string]`
           `password=[string]`          

        - Optional:
           `country=[string]`
           `endDate=[string]`
           `startDate=[string]`
           `export=[string]`

* **Conditions:**
        Date is in the format YYYY-MM-DD
</details>
