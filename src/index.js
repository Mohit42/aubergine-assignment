const express = require('express');
const mongoose = require('mongoose');
const app = express();
const { PORT } = require('./shared/config.json');
const userRoute = require('./controllers/users');
const covidRoute = require('./controllers/covid');

const {
    authenticate
} = require('./middlewares/user')
const {
    generalError
} = require('./middlewares/error');
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.use('/covid', authenticate);

mongoose.connect('mongodb://localhost/aubergineUserInfo', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});

app.use('/user', userRoute);
app.use('/covid', covidRoute);

app.use(generalError);

app.listen(PORT);