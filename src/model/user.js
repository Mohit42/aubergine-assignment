const mongoose = require('mongoose');
var useerSchema = new mongoose.Schema({
    fisrt_name: String,
    last_name: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    }
});

var user = mongoose.model('user', useerSchema);
module.exports = user;