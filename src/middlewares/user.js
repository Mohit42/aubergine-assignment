const user = require('../model/user');
module.exports = {
    authenticate: function (req, res, next) {
        let params = req.body;
        user.findOne({
            email: params.email,
            password: params.password
        }, {
            country: 1
        }).then(result => {
            if (result) {
                params.country = params.country || result.country;
                next();
            } else {
                const err = new Error('Not authorized');
                err.status = 'Not authorized';
                err.statusCode = 404;
                next(err);
            }
        }).catch(err => {
            console.log(err);
            next(new Error());
        });
    }
}