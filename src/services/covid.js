const chartExporter = require('highcharts-export-server');
const nodemailer = require('nodemailer');
const https = require('https');
const moment = require('moment');
chartExporter.initPool();
const {
    SENDER_EMAIL_ID,
    SENDER_EMAIL_PASSWORD
} = require('../shared/config.json');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: SENDER_EMAIL_ID,
        pass: SENDER_EMAIL_PASSWORD
    }
});

module.exports = {
    getCovidData: function (params) {
        return new Promise((resolve, reject) => {
            https.get(params.url, res => {
                res.setEncoding("utf8");
                let covidData = "";
                res.on("data", data => {
                    covidData += data;
                });
                res.on("end", () => {
                    covidData = JSON.parse(covidData).data;
                    resolve(covidData)
                });
            }).on('error', err => {
                reject(err);
            });
        })
    },
    exportCovidData: function (params) {
        var exportSettings = {
            type: 'png',
            options: {
                chart: {
                    type: "column"
                },
                title: {
                    text: 'Covid confirmed cases'
                },
                xAxis: {
                    categories: params.labels
                },
                series: [{
                    data: params.data
                }]
            }
        };
        return new Promise((resolve, reject) => {
            chartExporter.export(exportSettings, (err, image) => {
                chartExporter.killPool();
                if (image) {
                    resolve(image);
                } else {
                    reject(err);
                }
            });
        });
    },
    sendEmail: function (params) {
        var mailOptions = {
            from: SENDER_EMAIL_ID,
            to: params.email,
            subject: 'Covid Data visual',
            text: 'Thank you for using our services!',
            attachments: [{
                filename: 'Covid.png',
                content: Buffer.from(params.image, 'base64')
            }]
        }
        return new Promise((resolve, reject) => {
            transporter.sendMail(mailOptions, function (error, response) {
                if (error) {
                    reject(error);
                } else {
                    resolve(response);
                }
            });
        });
    },
    filterCovidDataByRange: function (params) {
        let timeline = params.timeline;
        if (!params.end) return timeline.slice(0, 15);
        let end = timeline.findIndex(e => moment(e.date).isSame(params.end));
        if (end != -1) {
            let start = timeline.findIndex(e => moment(e.date).isSame(params.start));
            if (start != -1) {
                return timeline.slice(end, start + 1);
            }
            return timeline.slice(end, end + 15);
        }
        return timeline.slice(0, 15)
    }
}