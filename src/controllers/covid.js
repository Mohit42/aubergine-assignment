const express = require('express');
const alpha2 = require('iso-3166-1-alpha-2');
const router = express.Router();
const {
    exportCovidData,
    sendEmail,
    filterCovidDataByRange,
    getCovidData
} = require('../services/covid');

router.get('/', async (req, response, next) => {
    let params = req.body;
    const code = alpha2.getCode(params.country);
    const url = 'https://corona-api.com/countries/' + code;
    getCovidData({
        url
    }).then(covidData => {
        covidData.timeline = filterCovidDataByRange({
            timeline: covidData.timeline,
            end: params.endDate,
            start: params.startDate
        });
        let labels = [];
        let data = [];
        for (let item of covidData.timeline) {
            labels.push(item.date);
            data.push(item.confirmed);
        }
        if (params.export && params.export == 'true') {
            exportCovidData({
                labels,
                data
            }).then((image, imageErr) => {
                if (image) {
                    sendEmail({
                        image: image.data,
                        email: params.email
                    }).then(() => {
                        response.status(200).send(covidData);
                    }).catch(err => {
                        console.log(err);
                        next(new Error());
                    });
                } else {
                    console.log(imageErr);
                    next(new Error());
                }
            });
        } else {
            response.status(200).send(covidData);
        }
    }).catch(err => {
        console.log(err);
        next(new Error());
    });
});
module.exports = router;